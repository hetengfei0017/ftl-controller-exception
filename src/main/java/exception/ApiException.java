package exception;

import model.IErrorCode;

/**
 * @author Ren Junzhou<br>
 * Description:<br>
 * @since 2017/07/06<br>
 * Modified By:
 */
public class ApiException extends RuntimeException {
    private Object[] args;

    private IErrorCode errorCode;

    public ApiException(IErrorCode errorCode, Throwable e) {
        super(errorCode.toString(), e);
        this.errorCode = errorCode;
    }

    public ApiException(IErrorCode errorCode) {
        super(errorCode.toString());
        this.errorCode = errorCode;
    }

    public ApiException(IErrorCode errorCode, Object[] args) {
        super(errorCode.toString());
        this.errorCode = errorCode;
        this.args = args;
    }

    public ApiException(IErrorCode errorCode, Throwable e, Object[] args) {
        super(errorCode.toString(), e);
        this.errorCode = errorCode;
        this.args = args;
    }

    public ApiException(Throwable e) {
        super(e);

    }

    public IErrorCode getErrorCode() {
        return errorCode;
    }

    public Object[] getArgs() {
        return args;
    }

}