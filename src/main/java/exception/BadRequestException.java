package exception;

import model.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * @author Jin Lijuan<br>
 * Description: 400   BadRequestException  <br>
 * @since 2017/04/03<br>
 * Modified By: Yang Naihua
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends ApiException {
    public BadRequestException(ErrorCode error) {
        super(error);
    }
    public BadRequestException(ErrorCode error, Throwable e) {
        super(error, e);
    }
    public BadRequestException(ErrorCode errorCode, List<Object> args) {
        super(errorCode, args.toArray());
    }
}
