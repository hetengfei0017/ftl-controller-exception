package exception;

import model.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 未授权异常
 *
 * @author Jiantao Yan
 * @title: AuthenticationException
 * @date 2020/5/19 13:59
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class AuthException extends ApiException {

    public AuthException(ErrorCode error) {
        super(error);
    }

    public AuthException(ErrorCode errorCode, Throwable e) {
        super(errorCode, e);
    }

    public AuthException(ErrorCode errorCode, Object[] args) {
        super(errorCode, args);
    }

    public AuthException(Throwable e) {
        super(e);
    }
}
