package exception;

import model.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Jin Lijuan<br>
 * Description: 404   NotFoundException  <br>
 * @since 2017/02/13<br>
 * Modified By: Yang Naihua
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends ApiException {
    public NotFoundException(ErrorCode errorCode) {
        super(errorCode);
    }

    public NotFoundException(ErrorCode errorCode, Throwable e) {
        super(errorCode, e);
    }

    public NotFoundException(ErrorCode errorCode, Object[] args) {
        super(errorCode, args);
    }

    public NotFoundException(Throwable e) {
        super(e);
    }

}
