package exception;

import model.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * @author hetengfei
 * @version 1.0
 * @date 2020/8/4 11:49
 * @description
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ValidateException extends ApiException{
    public ValidateException(ErrorCode error) {
        super(error);
    }

    public ValidateException(ErrorCode errorCode, Throwable e) {
        super(errorCode, e);
    }

    public ValidateException(ErrorCode errorCode, Object[] args) {
        super(errorCode, args);
    }

    public ValidateException(ErrorCode errorCode, List<Object> args) {
        super(errorCode, args.toArray());
    }

    public ValidateException(Throwable e) {
        super(e);
    }
}
