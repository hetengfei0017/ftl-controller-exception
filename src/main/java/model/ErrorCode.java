package model;

public enum ErrorCode implements IErrorCode {
    CONFLICT_SUBSCRIBER,
    NOT_FOUND_SUBSCRIBER,
    PARAMS_CONVERT_FAIL,
    VALIDATA_PARAMETER_FAIL,
    NOT_FOUND,
    PATCH_FAILED,
    BEAN_UTILS_ERROR,
    TOPIC_NOT_FOUND;
    ErrorCode(){}
    @Override
    public String getEnumName() {
        return this.toString();
    }
}
