package model;

/**
 * @author hetengfei
 * @version 1.0
 * @date 2020/8/5 13:53
 * @description
 */
public interface IErrorCode {
    /**
     * 需要重写getEnumName方法，该方法用来返回枚举值，自定义类的话需要自定义枚举值并返回
     * @return
     */
    String getEnumName();
    // TODO 加个Http状态
//    String getHttpStatus();
}
