package handle;

import exception.ApiException;
import model.ApiError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.UUID;

/**
 * @author hetengfei
 * @version 1.0
 * @date 2020/8/14 16:08
 * @description
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private final MessageSource messageSource;
    @Autowired
    public GlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    public MessageSource getMessageSource() {
        return messageSource;
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> consumerApiException(ApiException e, HttpServletRequest request) {
        e.printStackTrace();
        ApiError apiError = buildApiError(e, request);
        ApiError apiErrorVO = buildApiErrorVO(apiError);
        return new ResponseEntity<ApiError>(apiErrorVO,apiError.getHttpStatus());
    }
    /**
     * 通过最底层的异常构建ApiError对象，并设置相关的Http状态码
     * @param ex
     * @param request
     * @return
     */
    private ApiError buildApiError(Exception ex, HttpServletRequest request) {
        ResponseStatus responseStatus = ex.getClass().getAnnotation(ResponseStatus.class);
        HttpStatus resultHttpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        if(responseStatus != null){
            resultHttpStatus = responseStatus.value();
        }
        ApiError apiError = new ApiError();
        apiError.setHttpStatus(resultHttpStatus);
        apiError.setId(UUID.randomUUID().toString().replace("-",""));
        apiError.setPath(request.getRequestURI());
        Throwable deepestCause = getStackDeepestCause(ex);
        apiError.setMessage(deepestCause.toString());
        apiError.setStackMessage(deepestCause.getMessage());
        apiError.setI18n(deepestCause.getMessage());
        if (ex instanceof ApiException) {
            ApiException apiException = (ApiException) ex;
            if (apiException.getErrorCode() != null) {
                String i18nMessage = this.getI18nMessage(apiException.getErrorCode().getEnumName(), apiException.getArgs(), ex.getLocalizedMessage(), request);
                apiError.setI18n(i18nMessage);
            }
        }
        return apiError;
    }

    /**
     * 获得最底层的异常对象。
     * @param ex
     * @return
     */
    private Throwable getStackDeepestCause(Throwable ex) {
        if(ex.getCause() == null){
            return ex;
        }
        Throwable cause = ex.getCause();
        return getStackDeepestCause(cause);
    }

    private String getI18nMessage(String message, Object[] args, String defaultMessage, HttpServletRequest request) {
        Locale locale = request.getLocale();
        if (locale == null) {
            locale = Locale.SIMPLIFIED_CHINESE;
        }
        String i18n = messageSource.getMessage(message, args, defaultMessage, locale);
        return StringUtils.isEmpty(i18n) ? message : i18n;
    }
    /**
     * 通过ApiError构建ApiError返回前端的对象，
     * @return
     */
    private ApiError buildApiErrorVO(ApiError apiError){
        ApiError apiErrorVO = new ApiError();
        apiErrorVO.setId(apiError.getId());
        apiErrorVO.setI18n(apiError.getI18n());
        apiErrorVO.setTimestamp(apiError.getTimestamp());
        return apiErrorVO;
    }
}
