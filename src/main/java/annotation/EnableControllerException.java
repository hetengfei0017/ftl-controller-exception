package annotation;

import handle.GlobalExceptionHandler;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author hetengfei
 * @version 1.0
 * @date 2020/8/13 13:32
 * @description
 */
@Target(ElementType.TYPE)
@Documented
@Import(GlobalExceptionHandler.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableControllerException {
}
